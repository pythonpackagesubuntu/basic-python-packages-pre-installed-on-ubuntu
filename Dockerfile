ARG DOCKER_BASE_IMAGE_PREFIX
ARG DOCKER_BASE_IMAGE_NAMESPACE=
ARG DOCKER_BASE_IMAGE_NAME=ubuntu
ARG DOCKER_BASE_IMAGE_TAG=devel
FROM ${DOCKER_BASE_IMAGE_PREFIX}${DOCKER_BASE_IMAGE_NAMESPACE}/${DOCKER_BASE_IMAGE_NAME}:${DOCKER_BASE_IMAGE_TAG}

ARG FIX_ALL_GOTCHAS_SCRIPT_LOCATION
ARG ETC_ENVIRONMENT_LOCATION
ARG CLEANUP_SCRIPT_LOCATION

# Depending on the base image used, we might lack wget/curl/etc to fetch ETC_ENVIRONMENT_LOCATION.
ADD $FIX_ALL_GOTCHAS_SCRIPT_LOCATION .
ADD $CLEANUP_SCRIPT_LOCATION .

RUN set -o allexport \
    && . ./fix_all_gotchas.sh \
    && set +o allexport \
    && apt-get update \
    && apt-get install --assume-yes python3-dev python3-pip python3-wheel git openssh-client \
    && if ls /usr/bin/python; then exit 1; fi \
    && ln -s /usr/bin/python3 /usr/bin/python \
    # When fix_all_gotchas.sh ran without Python installed, it didn't set up pip,
    # because it didn't find pip.
    && set -o allexport \
    && . ./fix_all_gotchas.sh \
    && set +o allexport \
    && python -m pip install --no-cache-dir --upgrade pip certifi \
    && . ./cleanup.sh
